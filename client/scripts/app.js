'use strict'

//App
const app = new Vue(
{
    el: '#wrapper',
    data:
    {
        config:
        {
            debug: null,
            version: null,
            hash: null
        },
        loggedIn: false,
        user:
        {
            username: null,
            email: null,
            emailConfirmed: null,
            profilePublic: null,
            platform: null,
            itemCollection: null,
            registerDate: null
        },
        loaded: false,
        search: "",
        searchString: "",
        page: "collection",
        data:
        {
            // Static
            static: {},

            // Items
            globalItems: {},            
            items: {},

            // Helminth
            helminth: {},
        },
        progression:
        {
            items: {},
            helminth: {}
        },
        completion:
        {
            items:
            {
                
            },
            helminth:
            {
                current: 0,
                count: 0,
                percent: 0,
            }            
        },
        context:
        {
            //Context info
            isOpen: false,
            type: null,
            position: { x: 0, y: 0 },

            //Item info
            iid: null,
            item: null,
            progression:
            {
                item: null,
                helminth: null
            }
        },
        pages:
        [
            {
                id: "collection",
                name: "Collection",
                image: "./images/icons/collection.png",
                default: true,
            },
            {
                id: "helminth",
                name: "Helminth",
                image: "./images/icons/helminth.png",
                default: false,
            },
            {
                id: "arcane",
                name: "Arcane",
                image: "./images/icons/arcane.png",
                default: false,
            },
        ],
        infoBox:
        {
            show: true,
            minimized: false,
            allowDismiss: false,
            type: "info-box-warning",
            text: "Warning: You are not logged in, so your changes are only saved locally and may be deleted by your browser.\nPlease log in or register to save your progress."
        },
        loginBox:
        {
            show: false,
            inProgress: false,
            values:
            {
                email: "",
                password: ""
            },
            alternates:
            {
                steam: true,
                xbox: true,
                discord: true
            },
            error: ""
        }
    },
    methods:
    {
        //
        // Events
        //

        loginMenuClick(e)
        {
            this.loginBox.show = !this.loginBox.show
        },

        loginLocalClick(e)
        {
            //Get values
            this.loginBox.error = ""
            const m = this.loginBox.values.email
            const p = this.loginBox.values.password
            
            //Guard
            if (m == '') //TODO
            {
                this.loginBox.error = "Email cannot be empty!"
                return
            }
            if (p == '') //TODO
            {
                this.loginBox.error = "Password cannot be empty!"
                return
            }

            //Send request
            this.loginBox.inProgress = true
            this.loginAsync(m, p)
        },

        searchClearClick(e)
        {
            this.search = ""
            this.searchChanged()
        },

        searchChanged(e)
        {
            this.searchString = this.search.toLowerCase()
        },

        itemClick(e)
        {
            //Locate target
            const t = e.target
            let itemId = t.parentNode.dataset.iid
            let collId = t.parentNode.parentNode.dataset.cid
            console.log("event:click", collId, itemId, 'item')

            //Update data
            this.updateItem(collId, itemId, null, 1)
        },

        partClick(e)
        {
            //Locate target
            const t = e.target
            let partId = t.dataset.pid
            let itemId = t.parentNode.dataset.iid
            let collId = t.parentNode.parentNode.dataset.cid
            console.log("event:click", collId, itemId, partId)

            //Update data
            this.updateItem(collId, itemId, partId, 1)
        },

        helminthClick(e)
        {
            //Locate target
            const t = e.target
            var itemId = t.parentNode.dataset.iid
            console.log("event:click", itemId)

            //Update data
            this.updateHelminth(itemId, 1)
        },

        pageNavClick(p)
        {
            this.switchPage(p)
        },

        collectionContextMenu(e)
        {
            //Search for item
            let target = e.target
            while (target != null)
            {
                if (target.classList.contains('item'))
                {
                    this.openItemContext(e, target)
                    break
                }
                target = target.parentElement
            }
            console.log('Context target:', target, '#collection')
        },

        helminthContextMenu(e)
        {
            //Search for item
            let target = e.target
            while (target != null)
            {
                if (target.classList.contains('item'))
                {
                    this.openHelminthContext(e, target)
                    break
                }
                target = target.parentElement
            }
            console.log('Context target:', target, '#helminth')
        },

        pageClick()
        {
            if (this.context.isOpen)
            {
                this.context.isOpen = false
            }

            if (this.loginBox.show)
            {
                this.loginBox.show = false
            }
        },

        toggleInfoBox(e)
        {
            if (this.infoBox.allowDismiss)
            {
                this.infoBox.show = false
            }
            else
            {
                this.infoBox.minimized = !this.infoBox.minimized
            }
        },

        //
        // Data updates
        //

        updateItem(collection, item, part, value)
        {
            if (part == null) //Item
            {
                //Update state
                this.progression.items[item].item = 
                    (this.progression.items[item].item == value) ? 0 : value
                console.log(this.progression.items[item])

                //Update section count
                if (this.progression.items[item].item == 1)
                {
                    this.updateItemCompletion(collection, 1)
                }
                else
                {
                    this.updateItemCompletion(collection, -1)
                }
            }
            else //Part
            {
                this.progression.items[item].parts[part] = 
                    (this.progression.items[item].parts[part] == value) ? 0 : value
            }

            //TODO
            //Save changes
            if (this.loggedIn) //Server
            {
                //this.saveToServerAsync(item)
            }
            else //Local
            {
                //this.saveToLocalStorageAsync()
            }
        },

        updateHelminth(item, value)
        {
            //Update state
            this.progression.helminth[item] = 
                (this.progression.helminth[item]) == value ? 0 : value

            //Update section count
            if (this.progression.helminth[item] == 1)
            {
                this.updateHelminthCompletion(1)
            }
            else
            {
                this.updateHelminthCompletion(-1)
            }

            //TODO
            //Save changes
            if (this.loggedIn) //Server
            {
                //this.saveToServerAsync(item)
            }
            else //Local
            {
                //this.saveToLocalStorageAsync()
            }
        },

        updateItemCompletion(collection, currentDelta)
        {
            const comp = this.completion.items[collection]

            comp.current += currentDelta
            comp.percent = ((comp.current / comp.count) * 100) | 0
        },

        updateHelminthCompletion(currentDelta)
        {
            const comp = this.completion.helminth

            comp.current += currentDelta
            comp.percent = ((comp.current / comp.count) * 100) | 0
        },

        //
        // Navigation
        //

        switchPage(page)
        {
            this.page = page
            console.log('Page:', page)
        },

        openItemContext(e, item)
        {
            //Item properties
            let iid = item.dataset.iid //Item ID
            
            //Set up context menu
            this.context.iid = iid
            this.context.item = this.data.globalItems[iid]
            this.context.progression.item = this.progression.items[iid]
            this.context.progression.helminth = this.progression.helminth[iid]

            //Set menu size
            const menuWidth = 600
            let bound = item.getBoundingClientRect()
            this.context.position.y = window.pageYOffset + bound.bottom + 15
            this.context.position.x = Math.min( bound.left, window.innerWidth - menuWidth)

            //Show context menu
            this.context.type = 'item'
            this.context.isOpen = true
        },

        openHelminthContext(e, item)
        {
            //Item properties
            let iid = item.dataset.iid
            
            //Set up context menu
            this.context.iid = iid
            this.context.item = this.data.globalItems[iid]
            this.context.progression.item = this.progression.items[iid]
            this.context.progression.helminth = this.progression.helminth[iid]

            const menuWidth = 600
            let bound = item.getBoundingClientRect()
            this.context.position.y = window.pageYOffset + bound.bottom - 50
            this.context.position.x = Math.min( bound.left, window.innerWidth - menuWidth)

            //Show context menu
            this.context.type = 'helminth'
            this.context.isOpen = true
        },

        partNameExtend(name)
        {
            return this.data.static.parts[name]
        },

        //
        // Storage
        //

        async saveToLocalStorageAsync() //TODO
        {
            let startTime = performance.now()

            //Collect
            let saved = {}
            for (let i in this.collection._global)
            {
                const item = this.collection._global[i]
                saved[i] = this.collection._global[i].state
            }
            
            //Save
            window.localStorage.setItem('framehub-collection', JSON.stringify(saved))

            //Performance
            let stopTime = performance.now()
            console.log(`Saving the collection took: ${ (stopTime - startTime) | 0 }ms`)
        },

        async loadFromLocalStorageAsync() //TODO
        {
            let startTime = performance.now()

            //Load from storage
            let collectionData = localStorage.getItem('framehub-collection')
            if (collectionData == null)
            {
                console.log('No collection found in local storage.')
                this.loaded = true
                return -1
            }

            //Verify integrity
            let collection 
            try
            {
                collection = JSON.parse(collectionData)
            }
            catch (error)
            {
                console.error('There was an error in collection data format, please clear cache.')
                return -2
            }

            //Load data
            for (let i in collection)
            {
                this.collection._global[i].state = collection[i]
            }

            //Update completion progression
            for (let i in this.collection)
            {
                if (i != "_global")
                {
                    //this.updateCompletion(i)
                }
            }

            //Performance
            let stopTime = performance.now()           
            return (stopTime - startTime) | 0
        },

        async saveToServerAsync(item) //TODO
        {
            let response = await fetch(this.config.client.routes.loginLocal,
            {
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({ username: user, password: pass })
            })

            let json = await response.json()
            if (json.ok == 0)
            {
                //Login failed
                this.loginBox.error = json.message
                this.loginBox.inProgress = false
                return
            }

            //Login successful
            if (this.config.debug) console.log(json.user)
        },

        async loadFromRemoteStorageAsync() //TODO
        {
            let startTime = performance.now()

            //Verify integrity
            let collection 
            try
            {
                collection = this.user.itemCollection
            }
            catch (error)
            {
                console.error('There was an error in collection data format, please contact an administrator.')
                return -1
            }

            //Load data
            for (let i in collection)
            {
                this.collection._global[i].state = collection[i]
            }

            //Update completion progression
            for (let i in this.collection)
            {
                if (i != "_global")
                {
                    //this.updateCompletion(i)
                }
            }

            //Performance
            let stopTime = performance.now()           
            return (stopTime - startTime) | 0
        },

        //
        // User
        //

        async loginAsync(mail, pass)
        {
            let response = await fetch(this.config.client.routes.loginLocal,
            {
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({ mail: mail, pass: pass })
            })

            let json = await response.json()
            if (json.ok == 0)
            {
                //Login failed
                this.loginBox.error = json.message
                this.loginBox.inProgress = false
                return
            }

            //Login successful
            if (this.config.debug) console.log(json.user)

            this.user = json.user
            this.loggedIn = true
            this.loginBox.inProgress = false
            this.loginBox.show = false
        }
    },
    filters:
    {
        partStateFilter(v)
        {
            return (v == 1) ? "active" : "" 
        },
        helminthSubsumeFilter(s)
        {
            return (s == 1) ? 'Subsumed' : 'Unsubsumed'
        },
        dismissTitleFilter(state)
        {
            return (state) ? 'Click to dismiss.' : 'Click to hide/show'
        }
    },
    created: async function()
    {
        console.log('App mounted successfully.')

        //Load config
        this.config = JSON.parse(document.getElementById('data-config').textContent)
        if (this.config.debug)
        {
            console.log('Debug mode enabled.')
        }


        //Log versions
        console.log('Version: ', this.config.version)
        console.log('Delpoy: ', this.config.hash)


        //Data reader
        function getData(id) { return JSON.parse(document.getElementById('data-' + id).textContent) }

        //Initialize static data
        for (let i of this.config.data.static)
        {
            this.$set(this.data.static, i, getData(i))
        }

        //Initialize items
        for (let i of this.config.data.items)
        {
            this.$set(this.data.items, i, getData(i))
            this.$set(this.completion.items, i,
            {
                current: 0,
                percent: 0,
                count: Object.keys(this.data.items[i]).length
            })
        }

        //Create global item list & progression
        for (let i of this.config.data.items)
        {
            for (let j in this.data.items[i])
            {
                const item = this.data.items[i][j]

                //Add global to global list
                this.data.globalItems[j] = item
                this.$set(this.data.globalItems, j, item)

                //Add progression
                const partNames = Object.keys(item.parts)                
                let parts = {}
                for (let k of partNames)
                {
                    parts[k] = 0
                }

                //Apply
                this.$set(this.progression.items, j,
                {
                    item: 0,
                    parts: parts
                })
            }
        }

        //Initialize helminth & progression
        this.data.helminth = getData('helminth')
        for (let i in this.data.helminth)
        {
            this.progression.helminth[i] = 0
        }
        this.completion.helminth.count = Object.keys(this.data.helminth).length

        //Load user
        this.user = getData('user')
        if (this.user)
        {
            this.loggedIn = true
            console.log('Loading remote collection...')
            //let time = await this.loadFromRemoteStorageAsync()
            //console.log(`Remote collection loaded in: ${ time }ms`)
        }
        else
        {
            console.log('Loading local collection...')
            //let time = await this.loadFromLocalStorageAsync()
            //console.log(`Local collection loaded in: ${ time }ms`)
        }

        //App loaded
        this.loaded = true
        window.setTimeout(() =>
        {
            document.getElementById('loading-overlay').style.display = 'none'
        }, this.config.debug ? 0 : this.config.client.overlayTime)

        console.log('App loaded.')
    }
})