'use strict'

//Modules
const fs = require('fs')

//Constants
const ignore = [ '.git', '.vs', 'node_modules', 'package-lock.json', 'vue.js' ]
const include = [ '.js', '.html', '.json', '.css', '.gitignore', '.md', '.yml', '.sh', '.ejs', '.txt' ]


function listFilesRecursive(path, list, dirs)
{
    let dir = fs.readdirSync(path)

    for (let i of dir)
    {
        if (ignore.includes(i)) continue

        const f = path + '/' + i
        let stat = fs.statSync(f)
        if (stat.isFile())
        {
            for (let j of include)
            {
                if (i.endsWith(j))
                {
                    list.push({ path: f, size: stat.size, ext: j })
                    break
                }                
            }            
        }
        if (stat.isDirectory())
        {
            dirs.push(i)
            listFilesRecursive(f, list, dirs)
        }
    }
}

//Calculate project size
function calculate(path)
{
    //Get all files
    let files = []
    let dirs = []
    listFilesRecursive(path, files, dirs)
    
    //File & dir counts
    console.log('File count: ', files.length)
    console.log('Directory count: ', dirs.length)

    //Count lines
    let data = {}
    for (let i of files)
    {
        if (!(i.ext in data))
        {
            data[i.ext] = { lines: 0, size: 0 }
        }

        //Add size
        data[i.ext].size += i.size

        //Count lines
        const lines = fs.readFileSync(i.path, 'utf8').split(/\r?\n/)
        data[i.ext].lines += lines.length
    }
    
    //Sum of all
    let dataSum = { lines: 0, size: 0 }
    for (let i in data)
    {
        dataSum.lines += data[i].lines
        dataSum.size += data[i].size
    }

    //Longest extension
    let longestExt = Object.keys(data).reduce((a, b) => a.length > b.length ? a : b ).length

    //Print result
    for (let i in data)
    {
        console.log('   ', i.padEnd(longestExt), '[', data[i].lines, '] (', data[i].size, ')')
    }

    console.log()
    console.log('   ALL [', dataSum.lines, '] (', dataSum.size, ')')
}

calculate(process.argv[2] || '.')