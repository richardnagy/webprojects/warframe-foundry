#Ubuntu focal web decelopment environment setup

#OS version
lsb_release -dc

#Update
sudo apt-get update -y
sudo apt-get upgrade -y

#Update command line prompt
cat ./terminal-prompt.sh >> ~/.bashrc

#Install Basic apps
sudo apt-get install curl -y
sudo apt-get install unzip -y
sudo apt-get install python3 -y

#Install Git
sudo apt-get install git -y
git --version
git config --global user.name "Richard Nagy"
git config --global user.email "nagy.richard.antal@gmail.com"
git config --global credential.helper store
git config --list

#Install Node
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install nodejs -y
sudo apt-get install nodemon -y
node -v || nodejs -v
sudo apt-get install npm -y
npm -v

#Install MongoDB
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo apt-get install -y mongodb-mongosh
sudo curl -o /etc/init.d/mongodb https://raw.githubusercontent.com/mongodb/mongo/master/debian/init.d
sudo chmod +x /etc/init.d/mongodb
sudo service mongodb start
sudo update-rc.d minidlna defaults

# Enable firewall
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https

sudo ufw enable
sudo ufw status

# Insall snapd
sudo apt install snapd
sudo snap install core
sudo snap refresh core

# Install certbot
sudo apt remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

#certbot certonly --manual