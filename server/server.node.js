'use strict'

// Load External Modules
const Express = require('express')
const Http = require('http')
const Https = require('https')
const Session = require('express-session')
const Path = require('path')
const Process = require("child_process")
const Mongoose = require('mongoose')
const Bcrypt = require('bcrypt')
const Crypto = require('crypto')
const Cookie = require('cookie-parser')
const Passport = require('passport')
const Steam = require('passport-steam')
const Hjson = require('hjson')
const Fs = require('fs')


// Install Hooks
require("hjson/lib/require-config")


// Load Config
const config = require('../config.hjson')
const credentials = require('./credentials.json')


// Run Commands
config.commitHash = String(Process.execSync(config.server.commands.deployHash)).trimEnd()
config.commitDate = new Date( String(Process.execSync(config.server.commands.deployDate)).trimEnd() )


// Constants
const clientStaticPath = Path.join(__dirname, config.server.paths.client)
const clientViewsPath = Path.join(__dirname, config.server.paths.views)
const TIME =
{
    second: 1000,
    minute: 1000 * 60,
    hour:   1000 * 60 * 60,
    day:    1000 * 60 * 60 * 24,
    week:   1000 * 60 * 60 * 24 * 7,
    month:  1000 * 60 * 60 * 24 * 30,
    year:   1000 * 60 * 60 * 24 * 365
}


// Load data
console.log('┌ Loading data...')
const loadJson = (name) => require(config.server.paths.data + name + '.json')
let data =
{
    static: {},
    items: {},
    helminth: {}
}
//static
for (let i of config.data.static)
{
    data.static[i] = loadJson(i)
}
console.log('│ - Static data loaded.')
//item
for (let i of config.data.items)
{
    data.items[i] = loadJson(i)
}
console.log('│ - Item data loaded.')
//helminth
data.helminth = loadJson(config.data.helminth)
console.log('│ - Helminth data loaded.')
console.log('┴ Done.')



// Create express server
console.log('┌ Initializing express server...')
const server = Express()
server.set('views', clientViewsPath)
server.set("view engine", "ejs")
if (config.debug)
{
    server.disable('view cache')
}
else
{
    server.enable('view cache')
}
console.log('┴ Done.')



// Intialize passport
console.log('┌ Initializing passport module...')
Passport.serializeUser((user, done) =>
{
    done(null, user)
})
Passport.deserializeUser((user, done) =>
{
    done(null, user)
})
console.log('│ - Serializers loaded.')

// Initiate Steam Strategy
Passport.use(new Steam.Strategy(
{
    returnURL: config.server.auth.steam.returnUrl,
    realm: config.server.auth.steam.realm,
    apiKey: 'B1D3DE05684DFA56B6A73146CF98700A'
},
(identifier, profile, done) =>
{
    process.nextTick(function ()
    {
        profile.identifier = identifier
        return done(null, profile)
    })
}))
console.log('│ - Steam module loaded.')
console.log('┴ Done.')



// Install express middleware
console.log('┌ Installing express middleware...')
server.use(Express.static(clientStaticPath, { dotfiles: 'allow' }))
console.log('│ + <Static>')
server.use(Express.urlencoded({ extended: true }))
console.log('│ + <URLEncoded>')
server.use(Express.json())
console.log('│ + <Json>')
server.use(Cookie())
console.log('│ + <Cookie>')
server.use(Session(
{
    secret: Crypto.randomBytes(config.server.session.secretSize).toString('base64').replace(/\W/g, ''),
    resave: config.server.session.resave,
    saveUninitialized: config.server.session.saveUninitialized,
    cookie:
    {
        secure: !config.server.debug,
        maxAge: TIME[config.server.session.maxAge]
    }
}))
console.log('│ + <Session>')
server.use(Passport.initialize())
server.use(Passport.session())
console.log('│ + <Passport>')

// Install custom middleware: InjectPipeline
server.use((req, res, next) =>
{
    req.internal =
    {
        data: data,
        config: config,
        time: Date.now,
    }
    next()
})
console.log('│ + <InjectPipeline> !custom')
console.log('┴ Done.')



// Install express routers
console.log('┌ Installing express routers...')
for (let router of config.server.routers)
{
    console.log(`│ > (${router.module}) -> {${router.url}}`)
    server.use(router.url, require(router.module))
}
if (config.debug)
{
    server.use('/debug', require('./routers/debug.router'))
}
console.log('┴ Done.')



// Connect to database
console.log('┌ Setting up database connection...')
let dburl = `${config.database.protocol}://${config.database.host}:${config.database.port}/${config.database.db}`
console.log(`│ > (${dburl})`)
const dbconnection = Mongoose.connect(dburl)
.then(() =>
{
    console.log('| ~ Connected to database!')
})
.catch(err =>
{
    console.error('| ! Connection to database failed, aborting...', err)
    process.abort()
})
console.log('│ ~ Connecting async... ')
Mongoose.set('bufferCommands', true)
console.log('│ > Settings updated.')
console.log('┴ Done.')



//Deploy info
console.log('┌ Deploy information:')
console.log('│ - Deploy Hash: ' + config.commitHash)
console.log('│ - Version: ' + config.server.version)
console.log('│ - Server path: ' + __dirname)
console.log('┴ Done.')



// Server start function
async function startServer({framework, protocol, host, port, options, server})
{
    //Wait for tasks before opening server
    console.log('┌ Awaiting required tasks...')    
    await dbconnection
    console.log('┴ Done.')

    //Start server
    console.log('┌ Opening web interface...')
    framework.createServer(options, server).listen(port)
    
    // Start logs
    console.log(`│ - Protocol: ${protocol}`)
    console.log(`│ - Host: ${host}`)
    console.log(`│ - Port: ${port}`)    
    console.log('┴ Done.')

    //Listening
    console.log('━━━━━━━━━━━━━━━━━━━')
    console.log('Server listening...')
    console.log('━━━━━━━━━━━━━━━━━━━', 'Runtime logs:')
}



// Start server
if (config.debug) // Debug mode
{
    //Start server
    startServer(
    {
        framework: Http,
        protocol: config.server.protocol,
        host: config.server.host,
        port: config.server.port,
        options: {},
        server: server
    })
}
else // Release mode
{
    //Load certificates
    var options =
    {
        key:  Fs.readFileSync(config.server.certificate.key, 'utf8'),
        cert: Fs.readFileSync(config.server.certificate.cert, 'utf8'),
        ca:   Fs.readFileSync(config.server.certificate.chain, 'utf8'),
    }

    //Start server
    startServer(
    {
        framework: Https,
        protocol: config.server.protocol,
        host: config.server.host,
        port: config.server.port,
        options: options,
        server: server
    })
}