'use strict'


// External Modules
const Mongoose = require('mongoose')
const Crypto = require('crypto')
const Bcrypt = require('bcrypt')


// Create model
const Model = Mongoose.model('user', new Mongoose.Schema(
{
    // Authentication
    auth:
    {
        // Local
        local:
        {
            type:
            {
                email: String,
                emailConfirmed: Boolean,
                password: String,
                emailConfirmToken: String
            },
            default: null
        },

        // Steam
        steam:
        {
            type:
            {
                steamId: String,
                profileUrl: String,
                avatarUrl: String
            },
            default: null
        }
    },

    // Npn-user modifiable properties
    registerDate:
    {
        type: Date,
        default: Date.now
    },
    lastLoginDate:
    {
        type: Date,
        default: Date.now
    },
    actions: Mongoose.Mixed,

    // User modifiable settings
    username:
    {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 32,
    },
    settings:
    {
        platform:
        {
            type: String,
            enum: [ 'pc', 'xb1', 'ps4', 'swi' ],
            default: 'pc',
            required: true
        },
        profilePublic:
        {
            type: Boolean,
            default: true
        }
    },
    
    //Completion map
    completion:
    {
        items:
        {
            type: Map,
            of:
            {
                status:
                {
                    type: Number,
                    required: true
                },
                parts:
                {
                    type: Map,
                    of: Number,
                    required: true
                },
                subsumed:
                {
                    type: Number
                }
            },
            default: {},
        },
        helminth:
        {
            type: Map,
            of:
            {
                type: Number,
                default: 0
            },
            default: {},
        }
    },
}), 'users')


// Create Model: Local
Model.createLocal = async (user) =>
{
    try
    {
        const token = Crypto.randomBytes(32).toString('base64').replace(/\W/g, '')
        const salt = await Bcrypt.genSalt(12)
        const hash = await Bcrypt.hash(user.password, salt)
        return new Model(
        {
            auth:
            {
                local:
                {
                    email: user.email,
                    emailConfirmToken: token,
                    password: hash,
                },
                steam: null
            },            
            username: user.username,
        })
    }
    catch (err)
    {
        console.warn("Error while creating Local user: ", err)
        return null
    }
}

// Create Model: Steam
Model.createSteam = (steam) =>
{
    try
    {
        return new Model(
        {
            auth:
            {
                local: null,
                steam:
                {
                    steamId: steam.id,
                    profileUrl: steam._json.profileurl,
                    avatarUrl: steam._json.avatarfull,
                }
            },
            username: steam.displayName,
        })
    }
    catch (err)
    {
        console.warn("Error while creating Steam user: ", err)
        return null
    }
}


// Expose Model
Model.expose = (user) =>
{
    let exposed =
    {
        auth:
        {
            local: null,
            steam: null
        },
        
        username: user.username,
        settings:
        {
            profilePublic: user.settings.profilePublic,
            platform: user.settings.platform,
        },
        
        completion: user.completion,
    }

    // Local authentication
    if (user.auth.local != null)
    {
        exposed.auth.local =
        {
            email:          user.auth.local.email,
            emailConfirmed: user.auth.local.emailConfirmed,
        }
    }

    // Steam authentication
    if (user.auth.steam != null)
    {
        exposed.auth.steam =
        {
            steamId:    user.auth.steam.steamId,
            profileUrl: user.auth.local.profileUrl,
            avatarUrl:  user.auth.local.avatarUrl,
        }
    }

    return exposed
}

// Export module
module.exports = Model