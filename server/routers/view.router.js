'use strict'

// Load Modules
const Express = require('express')
const router = Express.Router()


router.get('/', async (req, res) =>
{
    //Check user
    let user = null
    if (req.session.active)
    {
        user = req.session.user
    }

    //Send to render
    res.render("index",
    {
        config:
        {
            debug: req.internal.config.debug,
            version: req.internal.config.server.version,
            hash: req.internal.config.commitHash,
            date: req.internal.config.commitDate,
            client: req.internal.config.client,
            data: req.internal.config.data
        },
        data: req.internal.data,
        user: user,
    })
})

module.exports = router