'use strict'

// External Modules
const Express = require('express')


// Models
const LocalUserModel = require('../models/user.model')


// Create router
const router = Express.Router()


// Register
router.post('/users/delete', async (req, res) =>
{
    // Validate
    if (req.body.username == null && req.body.email == null)
    {
        return res.status(400).send("Must include username or email propery!")
    }

    if (req.body.username != null)
    {
        try
        {
            let res = await User.deleteOne({ username: req.body.username })
            console.log(res)
        }
        catch (error)
        {
            console.log(error)
            return res.status(500).send("Could not delete.")
        }
        return res.status(200).send("User deleted.")
    }
    else
    {
        try
        {
            let res = await User.deleteOne({ email: req.body.email })
            console.log(res)
        }
        catch (error)
        {
            console.log(error)
            return res.status(500).send("Could not delete.")
        }
        return res.status(200).send("User deleted.")
    }
})

module.exports = router