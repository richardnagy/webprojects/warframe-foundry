'use strict'

// External Modules
const Express = require('express')
const Bcrypt = require('bcrypt')
const Passport = require('passport')


// Models
const UserModel = require('../models/user.model')


// Create router
const router = Express.Router()


//Methods
function createSession(req, user)
{
    req.session.active = true
    req.session.uid = user._id
    req.session.user = user
}


// Login Local
router.post('/local', async (req, res) =>
{
    let mail = req.body.mail
    let pass = req.body.pass

    // Validate
    if (mail == null || pass == null)
    {
        return res.status(400).send({ ok: 0, message: 'Empty email or password!' })
    }

    // Check if this user exists
    let user = await UserModel.findOne(
    {
        'auth.local.email': mail
    })
    if (!user)
    {
        return res.status(200).send({ ok: 0, message: 'Invalid email or password' })
    }

    // Verify password
    let passMatch = Bcrypt.compareSync(pass, user.auth.local.password)
    if (!passMatch)
    {
        return res.status(200).send({ ok: 0, message: 'Invalid email or password' })
    }
    
    //Create session
    createSession(req, user)
    res.send(
    {
        ok: 1,
        user: UserModel.expose(user),
        redirect: '/'
    })

    console.log(`[ AUTH ] Local: ${user.username} (${user._id})`)
})

// Login Steam start
router.get('/steam', Passport.authenticate('steam', { failureRedirect: '/' }), (req, res) =>
{
    res.redirect('/')
})

// Login Steam success
router.get('/steam/return', Passport.authenticate('steam', { failureRedirect: '/' }), async (req, res) =>
{
    let steam = req.user

    //Check if user exists
    let user = await UserModel.findOne(
    {
        'auth.steam.steamId': steam.id
    })
    if (user) //User already registered
    {
        //Create session
        createSession(req, user)

        //TODO Update steam info

        console.log(`[ AUTH ] Steam: ${user.username} (${user._id})`)

        //Redirect to index
        return res.redirect('/')
    }

    //Create new user profile
    try
    {
        //Create user
        user = UserModel.createSteam(steam)
        await user.save()

        //Create session
        createSession(req, user)

        console.log(`[ SIGN ] Steam: ${user.username} (${user._id})`)
    }
    catch (error)
    {
        console.error(error)
    }

    return res.redirect('/')
})

module.exports = router