'use strict'

// External Modules
const Express = require('express')
const Bcrypt = require('bcrypt')


// Models
const UserModel = require('../models/user.model')


// Create router
const router = Express.Router()


// Register local
router.post('/', async (req, res) =>
{
    // Validate
    if (req.body.email == null)
    {
        return res.status(400).send({ ok: 0, message: 'Email not specified!', body: req.body })
    }

    // Check if email exists
    let user = await UserModel.findOne(
    {
        'auth.local.email': req.body.email
    })
    if (user)
    {
        return res.status(400).send({ ok: 0, message: 'This email address is already used!' })
    }
    
    // Create new user
    try
    {
        user = await UserModel.createLocal(
        {
            email: req.body.email,
            password: req.body.password,
            username: req.body.username
        })
        await user.save()
        console.log(`[ SIGN ] Local: ${user.username} (${user._id})`)
    }
    catch (error)
    {
        console.error(error)
        return res.status(500).send({ ok: 0, message: 'There was an error, please try again later!' })
    }

    res.send({ ok: 1, user: { username: user.username } })    
})

module.exports = router