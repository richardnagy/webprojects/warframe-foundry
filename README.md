# Warframe Hub

[[_TOC_]]

## Roadmap

- [ ] All items in collection
- [ ] Hub tracking completions
- [ ] Subsume (detailed helminth abilities, links)
- [ ] Arcanes
- [ ] Lich farming, (spawning, requiem stats)
- [ ] Market price on hover
- [ ] Farm zone on hover
- [ ] Relic price track
- [ ] World integration
   - [ ] Warn about invasion item
   - [ ] Warn about world cycle
   - [ ] Warn about Baro items


## Resources

| Internal | Link |
|---|---|
| Repository | https://gitlab.com/richard-nagy/warframe-foundry/ |

| Info | Link |
|---|---|
| Warframe Wiki |https://warframe.fandom.com/wiki/WARFRAME_Wiki |
| Font Awesome | https://fontawesome.com |

| Docs | Link |
|---|---|
| Express | https://expressjs.com/en/api.html |
| NodeJS 14 LTS | https://nodejs.org/dist/latest-v14.x/docs/api/ |
| Mongoose | https://mongoosejs.com/docs/ |
| Showdown `(.md->html)` | http://showdownjs.com |
| VSC Rest Client | https://marketplace.visualstudio.com/items?itemName=humao.rest-client |


## Arsenal

### Item Categories

|Name|Id|Categories
|---|---|---|
|Frame|`frame`| [`frame`] |
|Frame Primes|`frame-prime`|[`frame`, `prime`]|
|Primary|`primary`|[`weapon`, `primary`]|
|Primary Prime|`primary-prime`|[`weapon`, `primary`, `prime`]|
|Secondary|`primary`|[`weapon`, `secondary`]|
|Secondary Prime|`primary-prime`|[`weapon`, `secondary`, `prime`]|
|Melee|`melee`|[`weapon`, `melee`]|
|Melee Prime|`melee-prime`|[`weapon`, `melee`, `prime`]|
|Sentinel|`companion-sentinel`|[`companion`, `sentinel`]|
|Pet|`companion-pet`|[`companion`, `pet`]|
|Robotic Weapon|`companion-weapon`|[`companion`, `weapon`]|
|Archwing|`arch-wing`|[`arch`, `wing`]|
|Archgun|`arch-gun`|[`arch`, `gun`]|
|Archmelee|`arch-melee`|[`arch`, `melee`]|
|Necramech|`necramech`|[`necramech`, `operator`]|
|Modular: Kitgun|`modular-kitgun`|[`modular`, `kitgun`, `secondary`]
|Modular: Zaw|`modular-zaw`|[`modular`, `zaw`, `melee`]
|Modular: Amp|`modular-amp`|[`modular`, `amp`, `operator`]
|Modular: Moa|`modular-moa`|[`modular`, `companion`, `moa`]
|Modular: Hound|`modular-hound`|[`modular`, `companion`, `hound`]
|Modular: Infested Pet|`modular-ipet`|[`modular`, `companion`, `pet`, `infested`, `moa`]
|Modular: K-Drive|`modular-kdrive`|[`modular`, `kdrive`]|


### Category Graph

```mermaid
graph TD
    I{Item} --> F[Frame]
    F -->|Variant| FP[Frame Prime]

    I --> W{Weapon}
    W --> WP[Primary]
          WP -->|Variant| WPP[Primary Prime]
    W --> WS[Secondary]
          WS -->|Variant| WSP[Secondary Prime]
          WS -->|Include| MK
    W --> WM[Melee]
          WM -->|Variant| WMP[Melee Prime]

    I --> C{Companion}
    C --> CS[Sentinel]
          CS -->|Variant| CSP[Sentinel Prime]
    CS --> CW[Robotic Weapon]
    C --> CP{Pet}
    CP --> CPU[Kubrov]
    CP --> CPK[Kavat]
    CP --> MP

    I --> A{Arch}
    A --> AW[Archwing]
          AW -->|Variant| AWP[Archwing Prime]
    A --> AG[Archgun]
    A --> AM[Archmelee]    

    I --> M{Modular}
    M --> MK[Kitgun]
    M --> MZ[Zaw]
          MZ -->|Include| WM
    M --> MA[Amp]
    M --> MM[Moa]
          MM -->|Include| CP
          MM --> CW
    M --> MH[Hound]
          MH --> CW
    M --> MP[Infested Pet]
    M --> MD[K-Drive]

    I --> O{Operator}
    O --> N[Necramech]
          O -->|Include| MA

    I --> N{Nemesis}
    N --> NK{Kuva}
    N --> NT{Tenet}
```

### Item Variants

|Name|Category|Id|
|---|---|---|
| Normal | - | `normal` |
| MK1 | Tenno | `mk1` |
| Ceti | Tenno | `ceti` |
| Dragon | Tenno | `dragon` |
| Prime | Orokin | `prime` |
| Wraith | Grineer | `wraith` |
| Vandal | Corpus | `vandal` |
| Carmine | Corpus | `carmine` |
| Prisma | Baro | `prisma` |
| Mara | Baro | `mara` |
| Dex | Event | `dex` |
| Kuva | Nemesis | `kuva` |
| Tenet | Nemesis | `tenet` |
| Veykor | Syndicate | `meridian` |
| Telos | Syndicate | `hexis` |
| Synoid | Syndicate | `suda` |
| Secura | Syndicate | `perrin` |
| Rakta | Syndicate | `veil` |
| Sancti | Syndicate | `loka` |

### Variant Graph

```mermaid
graph TD
    V{Variant}
    V --> N[Normal]
    V --> T{Tenno}
            T --> TM[MK-1]
            T --> TC[Ceti]
    V --> O{Orokin}
            O --> OP[Prime]
    V --> G{Grineer}
            G --> GW[Wraith]
    V --> C{Corpus}
            C --> CV[Vandal]
            C --> CC[Carmine]
    V --> B{Baro}
            B --> BP[Prisma]
            B --> BM[Mara]
    V --> E{Event}
            E --> ED[Dex]
    V --> L{Nemesis}
            L --> LK[Kuva]
            L --> LT[Tenet]
    V --> S{Syndicate}
            S --> SM[Vaykor]
            S --> SH[Telos]
            S --> SS[Synoid]
            S --> SP[Secura]
            S --> SV[Rakta]
            S --> SL[Sancti]
```