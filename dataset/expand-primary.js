const fs = require('fs')
const crypto = require('crypto')


var items = JSON.parse( fs.readFileSync('./primary.json', 'utf8') )


for (const i in items)
{
    const item = items[i]

    items[i].partStates = {}
    for (const j in items[i].parts)
    {
        items[i].partStates[j] = 0
    }

    for (let k in items[i].parts)
    {
        items[i].parts[k] = [ items[i].parts[k] ]
    }

    items[i].links =
    {
        wiki: items[i].wiki,
        market: ""
    }
    delete items[i].market
    delete items[i].wiki

    items[i].state =
    {
        item: 0,
        parts: items[i].partStates
    }

    items[i].hash = crypto.createHash('md5').update(i).digest("hex")
    items[i].search = items[i].name.toLowerCase()
    items[i].type = 'primary'
    items[i].masteryLimit = -1
    items[i].variant = "normal"
}

fs.writeFileSync('../client/data/primary.json', JSON.stringify(items, null, "    "))

console.log("Exported to primary.json")