'use strict'

//Import
const fs = require('fs')
const crypto = require('crypto')


//Const
const infileFrame = './frame.txt'
const infilePrime = './frame-prime.txt'
const outfileFrame = '../client/data/frame.json'
const outfilePrime = '../client/data/frame-prime.json'


//Variable
let file = null
let json = {}


//Load files
file = fs.readFileSync(infileFrame, 'utf8')
let frames = file.split('\r\n')
file = fs.readFileSync(infilePrime, 'utf8')
let primes = file.split('\r\n')


//Create database
for (let i in frames)
{
    const name = frames[i]
    const id = 'frame-' + name.toLowerCase()

    json[id] = 
    {
        name: name,
        hash: crypto.createHash('md5').update(id).digest("hex"),
        image: id + '.png',
        type: "warframe",
        masteryLimit: 0,
        variant: "normal",
        variants: [ ],
        parts:
        {
            "blueprint":  [ "first", "second" ],
            "neuroptics": [ "first", "second" ],
            "chassis": [ "first", "second" ],
            "systems": [ "first", "second" ],
        },
        links:
        {
            wiki: "",
            market: "",
        },
        helminth:
        {
            name: "Lull",
            description: "Baruuk casts a calming wave over an area of 25 meters that lingers for 5 seconds. Enemies in sight or aware of Baruuk's presence that enter the area will gradually slow and then fall sleep for 20 seconds. Sleeping enemies awaken early when damaged, suffering disorientation for a brief moment, along with amnesia causing their alertness level to reset.",
            key: 2,
            wiki: "",
            augment: 
            {
                name: "Endless Lullaby",
                description: "Performing a finisher on a sleeping enemy will retrigger Lull for 100% of the remaining duration. Passive: +50% Lull Duration.",
                syndicates: [ "loka", "perrin" ],
                wiki: "",
                market: "",
            }
        },
        search: name.toLowerCase(),
        mastery: 6000,
        state:
        {
            item: 0,
            parts:
            {
                "blueprint": 0,
                "neuroptics": 0,
                "chassis": 0,
                "systems": 0
            },
            subsumed: 0,
        },
    }
}


let primeJson = {}
for (let i in primes)
{
    //Apply primes
    const nid = 'frame-' + primes[i].toLowerCase()
    json[nid].variants.push('prime')


    //Create prime json
    const pid = 'frame-prime-' + primes[i].toLowerCase()
    primeJson[pid] = JSON.parse(JSON.stringify(json[nid]))

    //Edit    
    delete primeJson[pid].helminth
    delete primeJson[pid].state.subsumed

    primeJson[pid].image = pid + '.png'
    primeJson[pid].variant = 'prime'
    primeJson[pid].variants = [ 'normal' ]
}


//Save
fs.writeFileSync(outfileFrame, JSON.stringify(json, null, "    "))
fs.writeFileSync(outfilePrime, JSON.stringify(primeJson, null, "    "))
console.log("Exported to", outfileFrame, outfilePrime)
