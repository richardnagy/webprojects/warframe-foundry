'use strict'

//Import
const fs = require('fs')
const crypto = require('crypto')


//Const
const infileFrame = '../server/data/frame.json'
const outfileFrame = '../server/data/frame.json'


//Variable
let file = null


//Load files
file = fs.readFileSync(infileFrame, 'utf8')
let frames = JSON.parse(file)


//Create database
for (let i in frames)
{
    if (frames[i].helminth.augment != null)
        frames[i].helminth.augment.usable = true
}


//Save
fs.writeFileSync(outfileFrame, JSON.stringify(frames, null, "    "))
console.log("Exported to", outfileFrame)
